package com.example.apiexample.base.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MetaDtoEntity implements Serializable {

    private static final long serialVersionUID = 772041533987090408L;
    @NonNull
    private final String responseTime;
    @NonNull
    private final String timestamp;
    private PaginationDtoEntity pagination;

    @Getter
    @Accessors(fluent = true)
    public static class MetaBuilder {

        private MetaDtoEntity meta;
        private String responseTime;
        private String timestamp;
        private PaginationDtoEntity pagination;

        private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

        private MetaBuilder() {}

        public static MetaDtoEntity of(Date startTime) {
            if (startTime != null) {
                return new MetaDtoEntity(
                        (new Date().getTime() - startTime.getTime()) + " ms",
                        formatter.format(startTime));
            } else {
                return new MetaDtoEntity(
                        "0 ms",
                        formatter.format(new Date()));
            }
        }
    }
}
