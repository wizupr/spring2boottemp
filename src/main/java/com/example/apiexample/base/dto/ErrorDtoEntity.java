package com.example.apiexample.base.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorDtoEntity implements Serializable {

    private static final long serialVersionUID = -6917276106758907826L;
    private String type;
    private String code;
    private String message;
}
