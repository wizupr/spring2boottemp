package com.example.apiexample.base.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LinksDtoEntity implements Serializable {

    private static final long serialVersionUID = 6823744335638688706L;
    private String first;
    private String prev;
    private String next;
    private String last;
}
