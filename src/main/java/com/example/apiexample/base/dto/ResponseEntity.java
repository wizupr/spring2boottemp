package com.example.apiexample.base.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseEntity<Data> implements Serializable {

    private static final long serialVersionUID = 8341268373321384518L;
    private boolean success;
    private Data data;
    private LinksDtoEntity links;
    private MetaDtoEntity meta;
    private ErrorDtoEntity error;
}


