package com.example.apiexample.base.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessPageDataEntity<Data> {
    private Data data;
    private PaginationDtoEntity page;
}
