package com.example.apiexample.base.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(description = "共用-分頁功能-提供分頁資訊,查詢範圍,筆數,以及最後資料更新日")
public class PaginationDtoEntity implements Serializable {

    private static final long serialVersionUID = 8881613136893244824L;
    @ApiModelProperty(value = "要查找頁數")
    private long currentPage;
    @ApiModelProperty(value = "總頁數")
    private long totalPages;
    @ApiModelProperty(value = "總筆數")
    private long totalItems;
    @ApiModelProperty(value = "每頁筆數")
    private long itemPerPage;
    @ApiModelProperty(value = "查詢開始日期")
    private String startSelectTime;
    @ApiModelProperty(value = "查詢結束日期")
    private String endSelectTime;
    @ApiModelProperty(value = "資料最後更新時間")
    private String lastUpdateDataTime;
}
