package com.example.apiexample.base.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Getter
@Setter
@Accessors(fluent = true)
public class WarningException extends IllegalStateException {

    private static final long serialVersionUID = -1509216342316519563L;
    private final String i18nKey;
    private Date startTime;
    private Long userId;
    private Long accountId;
    private String inputData;

    public WarningException(String i18nKey) {
        this.i18nKey = i18nKey;
    }
}
