package com.example.apiexample.base.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Getter
@Setter
@Accessors(fluent = true)
public class ResultGenerationException extends Exception {

    private static final long serialVersionUID = 7591739596874413962L;
    private Date startTime;

    public ResultGenerationException(Exception e, Date startTime) {
        super(e);
        this.startTime = startTime;
    }
}
