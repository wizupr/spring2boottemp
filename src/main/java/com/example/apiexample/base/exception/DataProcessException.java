package com.example.apiexample.base.exception;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Getter
@Setter
@Accessors(fluent = true)
public class DataProcessException extends Exception {

    private static final long serialVersionUID = 2460084417498702051L;
    private Date startTime;

    public DataProcessException(Exception e, Date startTime) {
        super(e);
        this.startTime = startTime;
    }
}
