package com.example.apiexample.base.service;

import com.example.apiexample.base.dto.MetaDtoEntity;
import com.example.apiexample.base.dto.ResponseEntity;

import java.util.Date;

public abstract class BaseResponseService<InputData> extends BaseService<InputData, Void, ResponseEntity> {

    @Override
    protected ResponseEntity generateResult(Void processedData, Date startTime) {
        ResponseEntity responseEntity = new ResponseEntity();
        responseEntity.setSuccess(true);
        responseEntity.setMeta(MetaDtoEntity.MetaBuilder.of(startTime));
        return responseEntity;
    }
}
