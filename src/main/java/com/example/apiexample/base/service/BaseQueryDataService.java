package com.example.apiexample.base.service;

import com.example.apiexample.base.dto.ResponseEntity;

public abstract class BaseQueryDataService<InputData, ProcessedData, Result> extends BaseService<InputData, ProcessedData, ResponseEntity<Result>> {
}
