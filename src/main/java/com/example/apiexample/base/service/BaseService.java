package com.example.apiexample.base.service;

import com.google.gson.Gson;
import com.example.apiexample.base.exception.DataProcessException;
import com.example.apiexample.base.exception.ResultGenerationException;
import com.example.apiexample.base.exception.WarningException;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;


@Slf4j
public abstract class BaseService<InputData, ProcessedData, Result> {
    public Result request(InputData inputData) throws WarningException, DataProcessException, ResultGenerationException { Date startTime = new Date();

        ProcessedData processedData;
        try {
            processedData = processData(inputData);
        } catch (Exception e) {
            if (e instanceof WarningException) {
                throw warningException((WarningException) e, startTime, inputData);
            } else {
                StringWriter stringWriter = new StringWriter();
                e.printStackTrace(new PrintWriter(stringWriter));
                String exception = stringWriter.toString();
                log.error(exception);
                throw new DataProcessException(e, startTime);
            }
        }

        Result result;
        try {
            result = generateResult(processedData, startTime);
        } catch (Exception e) {
            if (e instanceof WarningException) {
                throw warningException((WarningException) e, startTime, inputData);
            } else {
                StringWriter stringWriter = new StringWriter();
                e.printStackTrace(new PrintWriter(stringWriter));
                String exception = stringWriter.toString();
                log.error(exception);
                throw new ResultGenerationException(e, startTime);
            }
        }
        return result;
    }

    private WarningException warningException(WarningException warning, Date startTime,InputData inputData) {
        warning.startTime(startTime);
        if (inputData != null) {
            Gson gson = new Gson();
            warning.inputData(gson.toJson(inputData));
        }
        return warning;
    }

    protected abstract ProcessedData processData(InputData inputData) throws Exception;

    protected abstract Result generateResult(ProcessedData processedData, Date startTime);

}
