package com.example.apiexample.base.service;

import com.example.apiexample.base.dto.ResponseEntity;

public abstract class BaseImportDataService<InputData, ProcessedData, Result> extends BaseService<InputData, ProcessedData, ResponseEntity<Result>> {
}
