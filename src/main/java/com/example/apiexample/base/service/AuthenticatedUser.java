package com.example.apiexample.base.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * <p>This Object serves as a Principal to be stored in UsernamePasswordAuthenticationToken.principal field</p>
 *
 * <p>It could be fetched as controller's argument with @AuthenticationPrincipal annotation</p>
 * <p>Or it could be fetched with <code>SecurityContext.getAuthentication().getPrincipal()</code></p>
 */
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class AuthenticatedUser {

    @NonNull
    private final Long userId;
    private String account;
    private String userType;
    private final String userName;
    // TODO 可考慮新增 role 欄位，目前先把 group 當 role 來用
    private final String role;
    private final Long roleId;
}
