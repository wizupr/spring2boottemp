package com.example.apiexample.base.util;

import java.security.SecureRandom;

public class StringUtil {

    public static boolean isEmpty(String parameter) {
        return parameter == null || (parameter.trim().length() == 0);
    }

    public static boolean isEmail(String email) {
        String regex = null;
        return email.matches(regex);
    }

    public static String generateSalt(int byteLength) {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[byteLength];
        random.nextBytes(bytes);
        String salt = new String(bytes);
        return salt;
    }

    public static String camelToUnderline(String parameter) {
        String underline = "_";
        if (StringUtil.isEmpty(parameter)) {
            return "";
        }
        int length = parameter.length();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char c = parameter.charAt(i);
            if (Character.isUpperCase(c)) {
                stringBuilder.append(underline);
                stringBuilder.append(Character.toLowerCase(c));
            } else {
                stringBuilder.append(c);
            }
        }
        return stringBuilder.toString();
    }

    public static void main(String[] argv) {
        String salt = generateSalt(4);
        System.out.println(salt);
    }
}
