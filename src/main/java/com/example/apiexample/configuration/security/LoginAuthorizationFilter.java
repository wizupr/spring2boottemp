package com.example.apiexample.configuration.security;

import com.example.apiexample.base.util.StringUtil;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Slf4j
public class LoginAuthorizationFilter extends BasicAuthenticationFilter {

    public LoginAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(SecurityConfiguration.ACCESS_HEADER);
        String header_apikey = request.getHeader(SecurityConfiguration.ACCESS_HEADER_APIKEY);

        //以使用者的 access_token 做登入
        if (!StringUtil.isEmpty(header)) {
            UsernamePasswordAuthenticationToken authentication = getAuthentication(header);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        //以ApiKey 做登入
        if (!StringUtil.isEmpty(header_apikey)) {
            UsernamePasswordAuthenticationToken authentication = getAuthenticationByApiKey(header_apikey);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(String token) {
        try {
            // parse token to get user id
            Long userId = Long.valueOf(
                    Jwts.parser()
                            .setSigningKey(SecurityConfiguration.ACCESS_SECRET.getBytes())
                            .parseClaimsJws(token)
                            .getBody()
                            .getSubject());
            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList();
            return new UsernamePasswordAuthenticationToken(userId, null, authorities);
        } catch (Exception e) {
            if (e instanceof ExpiredJwtException) {
                log.warn(e.getLocalizedMessage());
            } else {
                e.printStackTrace();
            }
        }
        // no Authentication will cause 401
        return null;
    }


    private UsernamePasswordAuthenticationToken getAuthenticationByApiKey(String token) {
        try {
            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList();
            return new UsernamePasswordAuthenticationToken(token, null, authorities);
        } catch (Exception e) {
            if (e instanceof ExpiredJwtException) {
                log.warn(e.getLocalizedMessage());
            } else {
                e.printStackTrace();
            }
        }
        // no Authentication will cause 401
        return null;
    }
}
