package com.example.apiexample.configuration.security;

public class SecurityConfiguration {

    public static final String ACCESS_SECRET = "ACCESS_SECRET";
    public static final String ACCESS_HEADER = "access_token";
    public static final String ACCESS_HEADER_APIKEY = "Authorization";
}
