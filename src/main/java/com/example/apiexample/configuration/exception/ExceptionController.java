package com.example.apiexample.configuration.exception;

import com.example.apiexample.base.dto.ErrorDtoEntity;
import com.example.apiexample.base.dto.ResponseEntity;
import com.example.apiexample.base.exception.DataProcessException;
import com.example.apiexample.base.exception.ResultGenerationException;
import com.example.apiexample.base.exception.WarningException;
import com.example.apiexample.base.dto.MetaDtoEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@Slf4j
@ControllerAdvice
public class ExceptionController {

    private static final String ERROR = "error";
    private static final String WARNING = "warning";

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(WarningException.class)
    @ResponseBody
    public ResponseEntity handleWarningException(WarningException warning) {
        return responseOf(warning.startTime(), WARNING, messageSource.getMessage(warning.i18nKey(), null, getLocale()));
    }

    @ExceptionHandler({
            MissingServletRequestParameterException.class,
            BindException.class,
            MethodArgumentNotValidException.class})
    @ResponseBody
    public ResponseEntity handleParsingRequestParameterException(Exception error) {
        StringWriter stringWriter = new StringWriter();
        error.printStackTrace(new PrintWriter(stringWriter));
        String exception = stringWriter.toString();
        log.error(exception);
        return responseOf(null, ERROR, "-997: " + error.getMessage());
    }

    @ExceptionHandler(DataProcessException.class)
    @ResponseBody
    public ResponseEntity handleDataProcessException(DataProcessException error) {
        return responseOf(error.startTime(), ERROR, "-998: " + error.getMessage());
    }

    @ExceptionHandler(ResultGenerationException.class)
    @ResponseBody
    public ResponseEntity handleResultGenerationException(ResultGenerationException error) {
        return responseOf(error.startTime(), ERROR, "-999: " + error.getMessage());
    }

    private ResponseEntity responseOf(Date startTime, String type, String message) {
        ResponseEntity responseEntity = new ResponseEntity<>();
        responseEntity.setSuccess(false);
        responseEntity.setMeta(MetaDtoEntity.MetaBuilder.of(startTime));
        responseEntity.setError(errorOf(type, message));
        return responseEntity;
    }

    private ErrorDtoEntity errorOf(String type, String message) {
        ErrorDtoEntity errorDtoEntity = new ErrorDtoEntity();
        errorDtoEntity.setType(type);
        errorDtoEntity.setMessage(message);
        return errorDtoEntity;
    }
}
