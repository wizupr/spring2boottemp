package com.example.apiexample.dao.dbModel;

import lombok.Data;

import java.util.Date;

@Data
public class MaterialVisualAlgoOutPutDbModel {
    private String algoNo;
    private String conditionNo;
    private String groupNo;
    private String productName;
    private String vendorNo;
    private String UUID;
    private double vfMax;
    private double vfMin;
    private double vfAvg;
    private double waveLengthMin;
    private double waveLengthMax;
    private double chipLOPMin;
    private double chipLOPMax;
    private double top3Min;
    private double top3Mean;
    private double top3Std;
    private double top3Scale;
    private double top3Mode;
    private double top3Q1;
    private double top3Q2;
    private double top3Q3;
    private double top3Prob;
    private double top5Min;
    private double top5Mean;
    private double top5Std;
    private double top5Scale;
    private double top5Mode;
    private double top5Q1;
    private double top5Q2;
    private double top5Q3;
    private double top5Prob;
    private int totalCount;
    private String createUser;
    private Date createDate;
}
