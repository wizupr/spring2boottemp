package com.example.apiexample.dao.dbModel;

import lombok.Data;

import java.util.Date;

@Data
public class MaterialPredictionAlgoInputDbModel {
    private String conditionNo;
    private String UUID;
    private String productName;
    private double lop1aSetting1;
    private double spec1aSetting1;
    private double vf2aSetting1;
    private double lopRange;
    private double wdRange;
    private double vfRange;
    private double vfMax;
    private double vfMin;
    private double wdMax;
    private double wdMin;
    private double lopMax;
    private double lopMin;
    private int totalCount;
    private int groes;
    private double top3Target;
    private double top5Target;
    private String buildType;
    private String createUser;
    private Date createDate;
    private String updateUser;
    private Date updateDate;
}
