package com.example.apiexample.dao.mapper;

import com.example.apiexample.dao.dbModel.MaterialPredictionAlgoInputDbModel;
import com.example.apiexample.dao.dbModel.MaterialVisualAlgoOutPutDbModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MaterialAlgoMapper {
    List<MaterialPredictionAlgoInputDbModel> selectByConditionNo(MaterialVisualAlgoOutPutDbModel materialVisualAlgoOutPutDbModel);
    List<MaterialVisualAlgoOutPutDbModel> selectByAlgoIdAndConditionNo(MaterialVisualAlgoOutPutDbModel materialVisualAlgoOutPutDbModel);
}

