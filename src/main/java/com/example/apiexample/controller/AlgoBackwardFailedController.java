package com.example.apiexample.controller;

import com.example.apiexample.base.dto.ResponseEntity;
import com.example.apiexample.base.exception.DataProcessException;
import com.example.apiexample.base.exception.ResultGenerationException;
import com.example.apiexample.controller.common.ExcelUtils;
import com.example.apiexample.controller.inputModel.AlgoSimulationInput;
import com.example.apiexample.controller.viewModel.AlgoBackwardFailedViewModel;
import com.example.apiexample.service.AlgoSimulationBackwardFailedExcelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(tags = "DSDS")
@RequestMapping("/download")
@RestController
public class AlgoBackwardFailedController {

    @Autowired
    private AlgoSimulationBackwardFailedExcelService algoSimulationBackwardFailedExcelService;

    @ApiOperation(value = "SDSDS")
    @GetMapping("/algoCreateExcel")
    public void getAlgoCreateExcel(
            @NotNull @ApiParam(value = "演算法ID", required = true) @Valid @RequestParam(value = "AlgoId") String algoId,
            HttpServletResponse response)
            throws DataProcessException, ResultGenerationException {
        AlgoSimulationInput inputData = new AlgoSimulationInput();
        inputData.setAlgoId(algoId);
        ResponseEntity<AlgoBackwardFailedViewModel> returnData = algoSimulationBackwardFailedExcelService.request(inputData);
        AlgoBackwardFailedViewModel algoBackwardFailedViewModel = returnData.getData();

        String excelExamplePath = "/java_everlight/opt/excel/FSA_EXAMPLE.xlsx";
        String outPutXlsxFileName = algoBackwardFailedViewModel.getOutPutXlsxFileName();
        ExcelUtils excelUtils = new ExcelUtils();
        excelUtils.createExcelXlsx(response,outPutXlsxFileName,excelExamplePath,
                algoBackwardFailedViewModel.getMaterialVisualAlgoOutPutDbModelList(),
                algoBackwardFailedViewModel.getMaterialPredictionAlgoInputDbModelList());
    }
}
