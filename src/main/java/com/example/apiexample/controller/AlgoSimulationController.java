package com.example.apiexample.controller;

import com.example.apiexample.base.dto.ResponseEntity;
import com.example.apiexample.base.exception.DataProcessException;
import com.example.apiexample.base.exception.ResultGenerationException;
import com.example.apiexample.controller.inputModel.AlgoSimulationInput;
import com.example.apiexample.controller.viewModel.AlgoBackwardFailedViewModel;
import com.example.apiexample.controller.viewModel.AlgoCheckStatusViewModel;
import com.example.apiexample.service.AlgoSimulationBackwardFailedExcelService;
import com.example.apiexample.service.AlgoSimulationService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(tags = "SDS")
@RequestMapping("/")
@RestController
public class AlgoSimulationController {

    @Autowired
    private AlgoSimulationBackwardFailedExcelService algoSimulationBackwardFailedExcelService;
    @Autowired
    private AlgoSimulationService algoSimulationService;

    @ApiOperation(value = "ASDASD")
    @GetMapping("algoSimulation")
    public ResponseEntity<AlgoCheckStatusViewModel> getAlgoSimulation(
            @NotNull @ApiParam(value = "條件編號", required = true) @Valid @RequestParam(value = "ConditionNo") String conditionNo,
            @NotNull @ApiParam(value = "演算類型", required = true) @Valid @RequestParam(value = "AlgoType") String algoType)
            throws DataProcessException, ResultGenerationException {
        AlgoSimulationInput inputData = new AlgoSimulationInput();
        inputData.setConditionNo(conditionNo);
        inputData.setAlgoType(algoType);
        return algoSimulationService.request(inputData);
    }

    @ApiOperation(value = "SADAS")
    @GetMapping("/algoBSReturn")
    public ResponseEntity<AlgoBackwardFailedViewModel> getAlgoBSReturn(
            @NotNull @ApiParam(value = "演算法ID", required = true) @Valid @RequestParam(value = "AlgoId") String algoId)
            throws DataProcessException, ResultGenerationException {
        AlgoSimulationInput inputData = new AlgoSimulationInput();
        inputData.setAlgoId(algoId);
        return algoSimulationBackwardFailedExcelService.request(inputData);
    }
}
