package com.example.apiexample.controller.common;

public enum SystemSettingTypeCode {
    NONE(null),
    DATA_CLEAN_ETL_VISUAL_CSV("VISUAL"),
    DATA_CLEAN_ETL_TRAINING_CSV("TRAINING"),
    DATA_CLEAN_ETL_TRAINING_MAIN_CSV("MAIN"),
    DATA_CLEAN_ETL_TRAINING_CONDITION_CSV("CONDITION"),
    DATA_CLEAN_ETL_TRAINING_BINBLUE_CSV("BINBLUE"),
    DATA_CLEAN_ETL_CONFIG("ETL_CONFIG"),
    BUILD_MODEL("BUILDMODEL"),
    ALGO_TYPE("TYPE"),
    ALGO_CONFIG("ALGO_CONFIG"),
    ALGO_BACKWARD_SIMULATION("BACKWARDSIMULATION"),
    ALGO_FORWARD_SIMULATION("FORWARDSIMULATION"),
    ALGO_FORWARD_AUTO("FORWARDAUTO")
    ;

    private final String value;
    SystemSettingTypeCode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
