package com.example.apiexample.controller.common;

import com.example.apiexample.dao.dbModel.MaterialPredictionAlgoInputDbModel;
import com.example.apiexample.dao.dbModel.MaterialVisualAlgoOutPutDbModel;
import org.apache.poi.xssf.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

public class ExcelUtils {

    public void createExcelXlsx(HttpServletResponse response, String outPutXlsxFileName,String excelExamplePath,
                                               List<MaterialVisualAlgoOutPutDbModel> materialAugmentationAlgoResultDbModelList,
                                               List<MaterialPredictionAlgoInputDbModel> materialPredictionAlgoInputDbModelList) {
        try {
            String disposition = "attachment; fileName="+outPutXlsxFileName;
            response.setContentType("application/octet-stream;charset=utf-8");
            response.setHeader("Content-Disposition", disposition);
            System.out.println("excelExamplePath = " + excelExamplePath);
            InputStream fileIn = new FileInputStream(excelExamplePath);
            XSSFWorkbook workbook = new XSSFWorkbook(fileIn);

            XSSFDataFormat format = workbook.createDataFormat();
            //EXECL內容欄位格式設定
            XSSFCellStyle csValuePreset = workbook.createCellStyle();
            csValuePreset.setDataFormat(format.getFormat("@"));
            //EXECL內容欄位百分比格式設定
            XSSFCellStyle csValuePercentage = workbook.createCellStyle();
            csValuePercentage.setDataFormat(format.getFormat("0.00%"));
            //EXECL內容欄位double數字格式設定
            XSSFCellStyle csValueDouble = workbook.createCellStyle();
            csValueDouble.setDataFormat(format.getFormat("#,##0.0000"));
            //EXECL內容欄位int數字格式設定
            XSSFCellStyle csValueInteger = workbook.createCellStyle();
            csValueInteger.setDataFormat(format.getFormat("#,##0"));

            XSSFSheet outputSheet = workbook.getSheetAt(0);
            XSSFSheet inputSheet  = workbook.getSheetAt(1);

            int sheet1RowNumber = 1;
            for (MaterialVisualAlgoOutPutDbModel materialVisualAlgoOutPutDbModel : materialAugmentationAlgoResultDbModelList) {
                XSSFRow sheet1Row = outputSheet.createRow(sheet1RowNumber);
                createCellValue(csValuePreset, sheet1Row, 0, materialVisualAlgoOutPutDbModel.getConditionNo(), "str");
                createCellValue(csValuePreset, sheet1Row, 1, materialVisualAlgoOutPutDbModel.getProductName(), "str");
                createCellValue(csValuePreset, sheet1Row, 2, materialVisualAlgoOutPutDbModel.getVendorNo(), "str");
                createCellValue(csValueDouble, sheet1Row, 3, materialVisualAlgoOutPutDbModel.getWaveLengthMin(), "double");
                createCellValue(csValueDouble, sheet1Row, 4, materialVisualAlgoOutPutDbModel.getWaveLengthMax(), "double");
                createCellValue(csValueDouble, sheet1Row, 5, materialVisualAlgoOutPutDbModel.getChipLOPMin(), "double");
                createCellValue(csValueDouble, sheet1Row, 6, materialVisualAlgoOutPutDbModel.getChipLOPMax(), "double");
                createCellValue(csValueDouble, sheet1Row, 7, materialVisualAlgoOutPutDbModel.getVfMin(), "double");
                createCellValue(csValueDouble, sheet1Row, 8, materialVisualAlgoOutPutDbModel.getVfMax(), "double");
                createCellValue(csValueDouble, sheet1Row, 9, materialVisualAlgoOutPutDbModel.getVfAvg(), "double");
                createCellValue(csValueDouble, sheet1Row, 10, materialVisualAlgoOutPutDbModel.getTop3Prob(), "double");
                createCellValue(csValueDouble, sheet1Row, 11, materialVisualAlgoOutPutDbModel.getTop5Prob(), "double");
                createCellValue(csValueInteger,sheet1Row, 12, materialVisualAlgoOutPutDbModel.getTotalCount(), "int");
                createCellValue(csValueDouble, sheet1Row, 13, materialVisualAlgoOutPutDbModel.getTop3Min(), "double");
                createCellValue(csValueDouble, sheet1Row, 14, materialVisualAlgoOutPutDbModel.getTop3Scale(), "double");
                createCellValue(csValueDouble, sheet1Row, 15, materialVisualAlgoOutPutDbModel.getTop3Mean(), "double");
                createCellValue(csValueDouble, sheet1Row, 16, materialVisualAlgoOutPutDbModel.getTop3Std(), "double");
                createCellValue(csValueDouble, sheet1Row, 17, materialVisualAlgoOutPutDbModel.getTop3Mode(), "double");
                createCellValue(csValueDouble, sheet1Row, 18, materialVisualAlgoOutPutDbModel.getTop3Q1(), "double");
                createCellValue(csValueDouble, sheet1Row, 19, materialVisualAlgoOutPutDbModel.getTop3Q2(), "double");
                createCellValue(csValueDouble, sheet1Row, 20, materialVisualAlgoOutPutDbModel.getTop3Q3(), "double");
                createCellValue(csValueDouble, sheet1Row, 21, materialVisualAlgoOutPutDbModel.getTop5Min(), "double");
                createCellValue(csValueDouble, sheet1Row, 22, materialVisualAlgoOutPutDbModel.getTop5Scale(), "double");
                createCellValue(csValueDouble, sheet1Row, 23, materialVisualAlgoOutPutDbModel.getTop5Mean(), "double");
                createCellValue(csValueDouble, sheet1Row, 24, materialVisualAlgoOutPutDbModel.getTop5Std(), "double");
                createCellValue(csValueDouble, sheet1Row, 25, materialVisualAlgoOutPutDbModel.getTop5Mode(), "double");
                createCellValue(csValueDouble, sheet1Row, 26, materialVisualAlgoOutPutDbModel.getTop5Q1(), "double");
                createCellValue(csValueDouble, sheet1Row, 27, materialVisualAlgoOutPutDbModel.getTop5Q2(), "double");
                createCellValue(csValueDouble, sheet1Row, 28, materialVisualAlgoOutPutDbModel.getTop5Q3(), "double");
                sheet1RowNumber++;
            }

            int sheet2RowNumber = 1;
            for (MaterialPredictionAlgoInputDbModel materialPredictionAlgoInputDbModel : materialPredictionAlgoInputDbModelList) {
                XSSFRow sheet2Row = inputSheet.createRow(sheet2RowNumber);
                createCellValue(csValuePreset, sheet2Row, 0, materialPredictionAlgoInputDbModel.getConditionNo(), "str");
                createCellValue(csValuePreset, sheet2Row, 1, materialPredictionAlgoInputDbModel.getProductName(), "str");
                createCellValue(csValuePercentage, sheet2Row, 2, materialPredictionAlgoInputDbModel.getTop3Target(), "double");
                createCellValue(csValuePercentage, sheet2Row, 3, materialPredictionAlgoInputDbModel.getTop5Target(), "double");
                createCellValue(csValueDouble, sheet2Row, 4, materialPredictionAlgoInputDbModel.getLopRange(), "double");
                createCellValue(csValueDouble, sheet2Row, 5, materialPredictionAlgoInputDbModel.getWdRange(), "double");
                createCellValue(csValueDouble, sheet2Row, 6, materialPredictionAlgoInputDbModel.getVfRange(), "double");
                createCellValue(csValueDouble, sheet2Row, 7, materialPredictionAlgoInputDbModel.getVfMax(), "double");
                createCellValue(csValueDouble, sheet2Row, 8, materialPredictionAlgoInputDbModel.getVfMin(), "double");
                createCellValue(csValueDouble, sheet2Row, 9, materialPredictionAlgoInputDbModel.getWdMax(), "double");
                createCellValue(csValueDouble, sheet2Row, 10, materialPredictionAlgoInputDbModel.getWdMin(), "double");
                createCellValue(csValueDouble, sheet2Row, 11, materialPredictionAlgoInputDbModel.getLopMax(), "double");
                createCellValue(csValueDouble, sheet2Row, 12, materialPredictionAlgoInputDbModel.getLopMin(), "double");
                createCellValue(csValueDouble, sheet2Row, 13, materialPredictionAlgoInputDbModel.getSpec1aSetting1(), "double");
                createCellValue(csValueDouble, sheet2Row, 14, materialPredictionAlgoInputDbModel.getLop1aSetting1(), "double");
                createCellValue(csValueDouble, sheet2Row, 15, materialPredictionAlgoInputDbModel.getVf2aSetting1(), "double");
                createCellValue(csValueInteger, sheet2Row, 16, materialPredictionAlgoInputDbModel.getTotalCount(), "int");
                createCellValue(csValueInteger, sheet2Row, 17, materialPredictionAlgoInputDbModel.getGroes(), "int");
                sheet2RowNumber++;
            }

            workbook.write(response.getOutputStream());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.flush();
            toClient.close();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createCellValue(XSSFCellStyle csType, XSSFRow valueRow, int number, Object value, String type) {
        XSSFCell valueCell = valueRow.createCell(number);
        valueCell.setCellStyle(csType);
        if ("str".equals(type)) {
            if (value != null) {
                valueCell.setCellValue(String.valueOf(value));
            } else {
                valueCell.setCellValue("");
            }
        }
        if ("double".equals(type)) {
            if (value != null) {
                valueCell.setCellValue((Double) value);
            } else {
                valueCell.setCellValue(0.0);
            }
        }
        if ("int".equals(type)) {
            if (value != null) {
                valueCell.setCellValue((Integer) value);
            } else {
                valueCell.setCellValue(0);
            }
        }
    }
}


