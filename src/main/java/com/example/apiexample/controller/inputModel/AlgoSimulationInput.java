package com.example.apiexample.controller.inputModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "發起演算")
public class AlgoSimulationInput {

    @ApiModelProperty(value = "查詢條件代碼", required = true)
    private String conditionNo;
    @ApiModelProperty(value = "演算Id", required = true)
    private String algoId;
    @ApiModelProperty(value = "發起演算類別", required = true)
    private String algoType;
}
