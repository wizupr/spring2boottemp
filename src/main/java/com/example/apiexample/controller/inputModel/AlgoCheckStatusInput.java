package com.example.apiexample.controller.inputModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "查詢演算狀態")
public class AlgoCheckStatusInput {

    @ApiModelProperty(value = "演算Id", required = true)
    private String algoId;
}
