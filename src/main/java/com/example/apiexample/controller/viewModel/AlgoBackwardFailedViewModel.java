package com.example.apiexample.controller.viewModel;

import com.example.apiexample.dao.dbModel.MaterialPredictionAlgoInputDbModel;
import com.example.apiexample.dao.dbModel.MaterialVisualAlgoOutPutDbModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "TE")
public class AlgoBackwardFailedViewModel {
    private String outPutXlsxFileName;
    private List<MaterialVisualAlgoOutPutDbModel> materialVisualAlgoOutPutDbModelList;
    private List<MaterialPredictionAlgoInputDbModel> materialPredictionAlgoInputDbModelList;
}
