package com.example.apiexample.controller.viewModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "演算進度")
public class AlgoCheckStatusViewModel {
    @ApiModelProperty(value = "演算Id")
    private String algoNo;
    @ApiModelProperty(value = "演算進度")
    private int progress;
}
