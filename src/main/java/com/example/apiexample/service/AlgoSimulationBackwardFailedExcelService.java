package com.example.apiexample.service;

import com.example.apiexample.base.dto.MetaDtoEntity;
import com.example.apiexample.base.dto.PaginationDtoEntity;
import com.example.apiexample.base.dto.ProcessPageDataEntity;
import com.example.apiexample.base.dto.ResponseEntity;
import com.example.apiexample.base.service.BaseQueryDataService;
import com.example.apiexample.controller.inputModel.AlgoSimulationInput;
import com.example.apiexample.controller.viewModel.AlgoBackwardFailedViewModel;
import com.example.apiexample.dao.dbModel.MaterialPredictionAlgoInputDbModel;
import com.example.apiexample.dao.dbModel.MaterialVisualAlgoOutPutDbModel;
import com.example.apiexample.dao.mapper.MaterialAlgoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AlgoSimulationBackwardFailedExcelService extends BaseQueryDataService<AlgoSimulationInput,
        ProcessPageDataEntity<AlgoBackwardFailedViewModel>,AlgoBackwardFailedViewModel> {

    private MaterialAlgoMapper materialAlgoMapper;

    @Override
    public ProcessPageDataEntity<AlgoBackwardFailedViewModel> processData(AlgoSimulationInput inputEntity) {

        ProcessPageDataEntity<AlgoBackwardFailedViewModel> result = new ProcessPageDataEntity<>();

        AlgoBackwardFailedViewModel algoBackwardFailedViewModel = new AlgoBackwardFailedViewModel();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = new Date();
        Calendar endData = Calendar.getInstance();

        MaterialVisualAlgoOutPutDbModel materialVisualAlgoOutPutDbModel = new MaterialVisualAlgoOutPutDbModel();
        materialVisualAlgoOutPutDbModel.setAlgoNo(inputEntity.getAlgoId());

        List<MaterialPredictionAlgoInputDbModel> materialPredictionAlgoInputDbModelList = materialAlgoMapper.selectByConditionNo(materialVisualAlgoOutPutDbModel);

        List<MaterialVisualAlgoOutPutDbModel> materialVisualAlgoOutPutDbModelList = materialAlgoMapper.selectByAlgoIdAndConditionNo(materialVisualAlgoOutPutDbModel);

        algoBackwardFailedViewModel.setMaterialPredictionAlgoInputDbModelList(materialPredictionAlgoInputDbModelList);
        algoBackwardFailedViewModel.setMaterialVisualAlgoOutPutDbModelList(materialVisualAlgoOutPutDbModelList);
        algoBackwardFailedViewModel.setOutPutXlsxFileName(inputEntity.getAlgoId()+"_VisualAlgoOutPut.XLSX");
        result.setData(algoBackwardFailedViewModel);
        endData.setTime(startDate);
        endData.add(Calendar.DATE,+1);
        PaginationDtoEntity page = new PaginationDtoEntity();
        page.setStartSelectTime(sdf.format(startDate));
        page.setEndSelectTime(sdf.format(endData.getTime()));
        page.setLastUpdateDataTime(startDate.toString());
        result.setPage(page);
        return result;
    }

    @Override
    protected ResponseEntity<AlgoBackwardFailedViewModel> generateResult(ProcessPageDataEntity<AlgoBackwardFailedViewModel> processResult, Date startTime) {
        ResponseEntity<AlgoBackwardFailedViewModel> response = new ResponseEntity<>();
        response.setSuccess(true);
        response.setData(processResult.getData());
        MetaDtoEntity metaDtoEntity = MetaDtoEntity.MetaBuilder.of(startTime);
        metaDtoEntity.setPagination(processResult.getPage());
        response.setMeta(metaDtoEntity);
        return response;
    }
}
