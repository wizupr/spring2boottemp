package com.example.apiexample.service;

import com.example.apiexample.base.dto.MetaDtoEntity;
import com.example.apiexample.base.dto.PaginationDtoEntity;
import com.example.apiexample.base.dto.ProcessPageDataEntity;
import com.example.apiexample.base.dto.ResponseEntity;
import com.example.apiexample.base.service.BaseQueryDataService;
import com.example.apiexample.controller.inputModel.AlgoSimulationInput;
import com.example.apiexample.controller.viewModel.AlgoCheckStatusViewModel;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class AlgoSimulationService extends BaseQueryDataService<AlgoSimulationInput,
        ProcessPageDataEntity<AlgoCheckStatusViewModel>,AlgoCheckStatusViewModel> {

    private String everlightAPIBatPath;

    @Override
    public ProcessPageDataEntity<AlgoCheckStatusViewModel> processData(AlgoSimulationInput inputEntity) throws IOException {

        ProcessPageDataEntity<AlgoCheckStatusViewModel> result = new ProcessPageDataEntity<>();
        System.out.println("inputEntity = " + inputEntity.getConditionNo());
        AlgoCheckStatusViewModel algoCheckStatusViewModel = new AlgoCheckStatusViewModel();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = new Date();
        Calendar endData = Calendar.getInstance();
        endData.setTime(startDate);
        endData.add(Calendar.DATE,+1);
        if(inputEntity.getConditionNo() != null) {

            SimpleDateFormat sdfAlgo = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String algoId = sdfAlgo.format(new Date());
            String simulationCmd = "";

            if("FS".equals(inputEntity.getAlgoType())){
                simulationCmd = everlightAPIBatPath + "ForwardSimulation.bat " + inputEntity.getConditionNo() + " " + algoId;
            }

            if("BS".equals(inputEntity.getAlgoType())){
                simulationCmd = everlightAPIBatPath + "BackwardSimulation.bat " + inputEntity.getConditionNo() + " " + algoId;
            }
            System.out.println("simulationCmd = " + simulationCmd);
            Runtime.getRuntime().exec(simulationCmd);
            algoCheckStatusViewModel.setAlgoNo(algoId);
            algoCheckStatusViewModel.setProgress(0);
        }
        result.setData(algoCheckStatusViewModel);
        PaginationDtoEntity page = new PaginationDtoEntity();
        page.setStartSelectTime(sdf.format(startDate));
        page.setEndSelectTime(sdf.format(endData.getTime()));
        page.setLastUpdateDataTime(startDate.toString());
        result.setPage(page);
        return result;
    }

    @Override
    protected ResponseEntity<AlgoCheckStatusViewModel> generateResult(ProcessPageDataEntity<AlgoCheckStatusViewModel> processResult,
                                                                      Date startTime) {
        ResponseEntity<AlgoCheckStatusViewModel> response = new ResponseEntity<>();
        response.setSuccess(processResult.getData().getProgress() != 990);
        response.setData(processResult.getData());
        MetaDtoEntity metaDtoEntity = MetaDtoEntity.MetaBuilder.of(startTime);
        metaDtoEntity.setPagination(processResult.getPage());
        response.setMeta(metaDtoEntity);
        return response;
    }
}
